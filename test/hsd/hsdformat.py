#!/usr/bin/env python3
import sys
from hsd import HSDParser, HSDStreamFormatter, HSDFormatter

formatter = HSDFormatter()
parser = HSDParser(defattrib="unit")
streamformatter = HSDStreamFormatter(parser, formatter)
streamformatter.feed(sys.argv[1])
