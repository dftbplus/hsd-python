"""The HSD package implements various functionalities for creating and
querying the HSD (Human readable Structured Data) format.
"""

class HSDException(Exception):
    """Base class for exceptions in the HSD packages."""
    pass


class HSDQueryError(HSDException):
    """Base class for errors detected by the HSDQuery object.

    Parameters
    ----------
    msg : str
        Error message
    node : Element, optional
        HSD element where error occured.

    Attributes
    ----------
    filename : str
        Name of the file where error occured (or empty string).
    line : int
        Line where the error occurred (or -1).
    tag : str
        Name of the tag with the error (or empty string).
    """

    def __init__(self, msg="", node=None):
        super().__init__(msg)
        hsdattrib = node.hsdattrib if node is not None else {}
        if node is not None:
            self.tag = node.hsdattrib.get(HSDATTR_TAG, node.tag)
        else:
            self.tag = ""
        self.file = hsdattrib.get(HSDATTR_FILE, -1)
        self.line = hsdattrib.get(HSDATTR_LINE, None)


class HSDMissingTagException(HSDQueryError): pass
class HSDInvalidTagException(HSDQueryError): pass
class HSDInvalidTagValueException(HSDQueryError): pass
class HSDMissingAttributeException(HSDQueryError): pass
class HSDInvalidAttributeException(HSDQueryError): pass
class HSDInvalidAttributeValueException(HSDQueryError): pass


class HSDParserError(HSDException):
    """Base class for parser related errors."""
    pass


def unquote(txt):
    """Giving string without quotes if enclosed in those."""
    if len(txt) >= 2 and (txt[0] in "\"'") and txt[-1] == txt[0]:
        return txt[1:-1]
    else:
        return txt


HSDATTR_PROC = "processed"
HSDATTR_EQUAL = "equal"
HSDATTR_FILE = "file"
HSDATTR_LINE = "line"
HSDATTR_TAG = "tag"